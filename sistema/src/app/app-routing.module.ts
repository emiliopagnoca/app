import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroComponent } from './pages/cadastro/cadastro.component';
import { RelatorioComponent } from './pages/relatorio/relatorio.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { path: "app-cadastro", component: CadastroComponent },
  { path: "app-cadastro/:id", component: CadastroComponent },
  { path: "app-relatorio", component: RelatorioComponent },
  { path: "**", component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
