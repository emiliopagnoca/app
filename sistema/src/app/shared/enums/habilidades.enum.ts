export enum HabilidadesEnum {
  CSharp = 0,
  Java = 1,
  Angular = 2,
  SQL = 3,
  ASP = 4
}
