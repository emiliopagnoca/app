import { SexoEnum } from "../../shared/enums/sexo.enum";
import { HabilidadesEnum } from "../../shared/enums/habilidades.enum";

export class ColaboradorEntity {
  id: number;
  nome: string;
  nascimento: Date;
  sexo: SexoEnum;
  email: string;
  habilidades: HabilidadesEnum[];
  ativo: boolean;
}
