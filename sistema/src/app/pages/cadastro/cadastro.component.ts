import { Component, OnInit, NgZone } from '@angular/core';
import { Validators, FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { HabilidadesEnum } from '../../shared/enums/habilidades.enum';
import { CadastroService } from './cadastro.service';
import { Router, Params, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})

export class CadastroComponent implements OnInit {

  colaboradorId: number;
  colaboradorAtivo: boolean;
  modoEditor: boolean = false;
  formGrupo: FormGroup;
  formEnviado: boolean = false;
  listaHabilidades: any = Object.keys(HabilidadesEnum).filter(key => !isNaN(Number(HabilidadesEnum[key])));

  constructor(private formBuilder: FormBuilder, private cadastroService: CadastroService,
    private ngZone: NgZone, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.formGrupo = this.formBuilder.group({
      id: [],
      nome: [null, [Validators.required, Validators.pattern(/[a-zA-z]+\s[a-zA-z]+\s[a-zA-z]+/)]],
      email: [null, [Validators.email]],
      nascimento: [null, [Validators.required, this.validarIdade]],
      sexo: [null, [Validators.required]],
      habilidades: [null, [Validators.required]],
      ativo: []
    });

    this.route.params.subscribe((params: Params) => this.colaboradorId = params['id']);

    if (this.colaboradorId && this.colaboradorId > 0) {
      this.modoEditor = true;
      this.cadastroService.obterColaborador(this.colaboradorId)
        .subscribe(data => {
          if (data) {
            this.formGrupo.controls.id.setValue(data['id']);
            this.formGrupo.controls.nome.setValue(data['nome']);
            this.formGrupo.controls.email.setValue(data['email']);
            this.formGrupo.controls.nascimento.setValue(new Date(data['nascimento']).toISOString().slice(0, 10));
            this.formGrupo.controls.sexo.setValue(data['sexo']);
            this.formGrupo.controls.habilidades.setValue(data['habilidades']);
            this.formGrupo.controls.ativo.setValue(data['ativo']);
            this.colaboradorAtivo = data['ativo'];
            this.formGrupo.controls.nome.setValidators([Validators.required, Validators.pattern(/[a-zA-z]+\s[a-zA-z]+\s[a-zA-z]+/)]);

          }
        });
    }


  }

  validarIdade(control: AbstractControl): { [key: string]: boolean } | null {
    const dataAtual = new Date();
    const dataNascimento = new Date(control.value);
    let idade = dataAtual.getFullYear() - dataNascimento.getFullYear();
    const m = dataAtual.getMonth() - dataNascimento.getMonth();

    if (m < 0 || (m === 0 && dataAtual.getDate() < dataNascimento.getDate()))
      idade--;

    if (idade !== undefined && (isNaN(idade) || idade < 18)) {
      return { 'ageRange': true };
    }

    return null;
  }

  get f() { return this.formGrupo.controls; }

  enviar() {
    this.formEnviado = true;
    this.formGrupo.value['ativo'] = true;

    if (this.formGrupo.invalid) {
      return;
    }

    if (this.modoEditor) {
      this.cadastroService.editar(this.formGrupo.value)
        .subscribe(() => {
          window.alert('Cadastro editado com sucesso!');
          this.ngZone.run(() => this.router.navigateByUrl('/app-relatorio'))
        });
    } else {
      this.cadastroService.cadastrar(this.formGrupo.value)
        .subscribe(() => {
          window.alert('Cadastro efetuado com sucesso!');
          this.ngZone.run(() => this.router.navigateByUrl('/app-relatorio'))
        });
    }

  }

  resetar() {
    this.formEnviado = false;
    this.formGrupo.reset();
  }

}
