import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ColaboradorEntity } from '../../shared/entities/colaborador.entity';

@Injectable({
  providedIn: 'root'
})
export class CadastroService {

  apiUrl: string = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) { }

  cadastrar(colaborador: ColaboradorEntity): Observable<any> {
    try {
      return this.http.post<ColaboradorEntity>(`${this.apiUrl}/colaborador`, colaborador, this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        )
    }
    catch (e) {
      throwError(e.message);
    }
  }

  obterColaborador(id: number): Observable<any> {
    try {
      return this.http.get<ColaboradorEntity>(`${this.apiUrl}/colaborador/${id}`, this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        )
    }
    catch (e) {
      throwError(e.message);
    }
  }

  editar(colaborador: ColaboradorEntity): Observable<any> {
    try {
      return this.http.put(`${this.apiUrl}/colaborador/${colaborador.id}`, colaborador, this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        )
    }
    catch (e) {
      throwError(e.message);
    }
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
