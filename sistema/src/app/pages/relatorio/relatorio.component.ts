import { Component, ViewChild } from '@angular/core';
import { RelatorioService } from './relatorio.service';
import { ColaboradorEntity } from '../../shared/entities/colaborador.entity';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SexoEnum } from '../../shared/enums/sexo.enum';
import { HabilidadesEnum } from '../../shared/enums/habilidades.enum';
import { Router } from '@angular/router';


@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.scss']
})

export class RelatorioComponent {

  colunas: string[] = ['nome', 'nascimento', 'idade', 'email', 'sexo', 'habilidades', 'status'];
  dataSource: MatTableDataSource<ColaboradorEntity>;
  SexoEnum = SexoEnum;
  HabilidadesEnum = HabilidadesEnum;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private relatorioService: RelatorioService, private router: Router) {
    this.relatorioService.obterDados()
      .subscribe(data => {
        if (data) {
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      });
  }

  aplicarFiltro(event: Event) {
    const filtrar = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filtrar.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  calcularIdade(nascimento: any): number {
    const dataAtual = new Date();
    const dataNascimento = new Date(nascimento);
    let idade = dataAtual.getFullYear() - dataNascimento.getFullYear();
    const m = dataAtual.getMonth() - dataNascimento.getMonth();

    if (m < 0 || (m === 0 && dataAtual.getDate() < dataNascimento.getDate()))
      idade--;

    return idade;
  }

  converterEnumArray(lista: any) {
    let listaEnum: string[] = [];
    lista.map(function (el) {
      listaEnum.push(HabilidadesEnum[el]);
    });
    return listaEnum;
  }

  excluir(colaborador: ColaboradorEntity) {
    this.relatorioService.excluirColaborador(colaborador.id)
      .subscribe(() => {
        window.alert('Cadastro excluído com sucesso!');
        const index = this.dataSource.data.indexOf(colaborador);
        this.dataSource.data.splice(index, 1);
        this.dataSource._updateChangeSubscription();
      });
  }

  editar(id: number) {
    this.router.navigate(["app-cadastro", id]);
  }

  ativarDesativar(colaborador: ColaboradorEntity) {
    let msg = colaborador.ativo == true ? "desativado" : "reativado";
    colaborador.ativo = !colaborador.ativo;

    this.relatorioService.ativarDesativar(colaborador)
      .subscribe(() => {
        window.alert(`Cadastro ${msg} com sucesso!`);
        const index = this.dataSource.data.indexOf(colaborador);
        this.dataSource.data[index].ativo = colaborador.ativo;
        this.dataSource._updateChangeSubscription();
      });
  }

}
