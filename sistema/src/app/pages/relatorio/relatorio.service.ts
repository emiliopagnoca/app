import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ColaboradorEntity } from '../../shared/entities/colaborador.entity';

@Injectable({
  providedIn: 'root'
})

export class RelatorioService {

  apiUrl: string = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) { }

  obterDados(): Observable<ColaboradorEntity[]> {
    try {
      return this.http.get<ColaboradorEntity[]>(`${this.apiUrl}/colaborador`, this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        )
    }
    catch (e) {
      throwError(e.message);
    }
  }

  excluirColaborador(id: number): Observable<any> {
    try {
      return this.http.delete(`${this.apiUrl}/colaborador/${id}`, this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        )
    }
    catch (e) {
      throwError(e.message);
    }
  }

  ativarDesativar(colaborador: ColaboradorEntity): Observable<any> {
    try {
      return this.http.put(`${this.apiUrl}/colaborador/${colaborador.id}`, colaborador, this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        )
    }
    catch (e) {
      throwError(e.message);
    }
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
